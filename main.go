package main

import (
	"fmt"
	"strconv"

	"gitlab.com/reinerh/dhbw/tel21a/gameboard"
)

func main() {
	runConnect4()
}

// Haupt-Spielfunktion. Führt das Spiel aus.
func runConnect4() {
	fmt.Println("Willkommen bei 4-Gewinnt")

	// Spielfeld erzeugen
	board := gameboard.NewBoard(6, 7, ' ')

	gameLoop(board)

}

// Erwartet ein Spielfeld und gibt es aus.
func printBoard(b []string) {
	for i := range b[0] {
		fmt.Printf("| %v ", i+1)
	}
	fmt.Println("|")
	for i := range b[0] {
		_ = i
		fmt.Print("+---")
	}
	fmt.Println("+")
	for _, row := range b {
		fmt.Print("| ")
		for _, c := range row {
			fmt.Printf(string(c) + " | ")
		}
		fmt.Println()
	}
	for i := range b[0] {
		_ = i
		fmt.Print("+---")
	}
	fmt.Println()
}

// Führt die Hauptschleife des Spielers aus.
// Erwartet ein Spielfeld, mit dem gearbeitet werden soll.
func gameLoop(board []string) {
	currentPlayer := 'X'

	for !gameOver(board) {
		board = makeMove(board, currentPlayer)

		currentPlayer = nextPlayer(currentPlayer)
	}

	fmt.Print("Das Spiel ist beendet. Ergebnis: ")
	if isDraw(board) {
		fmt.Println("Unentschieden!")
	} else {
		fmt.Printf("Spieler %c gewinnt!\n", nextPlayer(currentPlayer))
	}
}

// Erwartet ein Spielfeld und liefert true, falls das Spiel beendet ist.
func gameOver(board []string) bool {
	return isDraw(board) || playerWins(board, 'X') || playerWins(board, 'O')
}

// Liefert true, wenn der gegebene String (input) eine Ziffer zwischen 1 und der Breite
// des Spielfelds ist.
func inputValid(board []string, input string) bool {
	columnCount := len(board)
	inputAsInt, error := strconv.Atoi(input)
	return error == nil && inputAsInt >= 1 && inputAsInt <= columnCount
}

// Liefert die tiefste Zeile, die in Spalte col noch ein Leerzeichen enthält.
// Liefert -1, wenn keine solche Zeile existiert.
func getLowestSpaceRow(board []string, col int) int {
	row := 0
	if board[row][col] != ' ' {
		return -1
	}
	for row < len(board) && board[row][col] == ' ' {
		row++
	}
	return row - 1
}

// Erwartet ein Spielfeld und den Namen des aktuellen Spielers.
// Fragt den Spieler nach einem Zug und trägt ihn im Spielfeld ein.
// Liefert das veränderte Spielfeld.
func makeMove(board []string, player rune) []string {

	// Spielfeld ausgeben und Spieler ansprechen.
	fmt.Println("Aktuelles Spielfeld:")
	fmt.Println()
	printBoard(board)
	fmt.Printf("Spieler %c, du bist am Zug.\n", player)

	// Eingabe vom Spieler einlesen und prüfen.
	columnCount := len(board[0])
	fmt.Printf("Wähle eine Spalte (1-%v): ", columnCount)
	var input string
	fmt.Scanln(&input) // Eingabe einlesen.
	if !inputValid(board, input) {
		fmt.Println("Die Eingabe war nicht erlaubt. Bitte noch einmal probieren.")
		fmt.Println()
		return makeMove(board, player)
	}

	// Gewählte Spalte aus Eingabe bestimmen.
	col, _ := strconv.Atoi(input)
	col--

	// Die Zeile bestimmen, in der das neue Zeichen stehen muss.
	row := getLowestSpaceRow(board, col)

	// Wenn row nach der obigen Schleife gültig ist, Zug durchführen.
	if row >= 0 {
		board = gameboard.Put(board, row, col, player)
		return board
	}

	fmt.Println("Die Eingabe war nicht erlaubt. Bitte noch einmal probieren.")
	fmt.Println()
	return makeMove(board, player)
}

// Erwartet den aktuellen Spieler und die Namen beider Spieler.
// Liefert den Spieler, der als nächstes am Zug ist.
func nextPlayer(currentPlayer rune) rune {
	if currentPlayer == 'X' {
		return 'O'
	}
	return 'X'
}

// Erwartet ein Spielfeld und liefert true, falls das Spiel unentschieden ist.
func isDraw(board []string) bool {
	return gameboard.NoneContains(board, ' ') && !playerWins(board, 'X') && !playerWins(board, 'O')
}

// Erwartet eine Spielfeld und einen Spielernamen.
// Liefert true, falls dieser Spieler gewonnen hat.
func playerWins(board []string, player rune) bool {
	return gameboard.AnyRowContainsNInARow(board, 4, player) || gameboard.AnyColumnContainsNInARow(board, 4, player) || gameboard.AnyDiagContainsNInARow(board, 4, player)
}
